#ifndef HERO_H
#define HERO_H
#include <allegro5/allegro.h>
#include <globals.h>
typedef ALLEGRO_BITMAP* BITMAP;

class Hero {
    public:
        Hero();
		
		/* Setters */
		void updateImage(uint8_t thread);
		void updatePosition(uint8_t thread);
		void updateStatus(uint8_t thread);
		void setKey(int keycode, bool state);
		
		/* Getters */
		BITMAP getImage();
		int16_t getX();
		int16_t getY();
		int16_t getDir();

        
    private:
       	BITMAP IDLE, WALK[6], CROUCH[3], STAND, DMG[2][3], JUMP[3], STRIKE_V[3][3], STRIKE_H[3][3], STRIKE_J[3][3], currentImage = NULL;
		uint8_t WIDTH = 32, HEIGHT = 64;
		
		/* Req 2.5-1 */
		bool keyState[9] = {false, false, false, false, false, false, false, false, false};
		int keyMap[9] = {ALLEGRO_KEY_UP, ALLEGRO_KEY_DOWN, ALLEGRO_KEY_LEFT, ALLEGRO_KEY_RIGHT, ALLEGRO_KEY_SPACE, ALLEGRO_KEY_Z, ALLEGRO_KEY_X, ALLEGRO_KEY_C, ALLEGRO_KEY_ESCAPE}; 
        
        
		/* Req 3.1-1A */
		DIR::STATE faceDir = DIR::RIGHT;
		int16_t xPos = 96, yPos = 300;
		STATUS::STATE moveType = STATUS::STANDING;
		int16_t statusData[2] = {0, 0};
		ALLEGRO_TIMER *statusTimer = NULL;
		
		
		int16_t jumpTime = 750, jumpHeight = 100, moveSpeed = 2;
	
};

#endif

#ifndef GLOBALS_H
#define GLOBALS_H

#define PLAYER_SPRITE_PATH "../doc/resources/maleBase/full/advnt_full.png"

namespace DIR {
	typedef enum { LEFT = ALLEGRO_FLIP_HORIZONTAL, RIGHT = 0 } STATE;
}

namespace STATUS {
	typedef enum { JUMPING, STANDING, CROUCHING } STATE;
}


namespace KEY {
	typedef enum { UP, DOWN, LEFT, RIGHT, BLOCK, STRIKE_VRT, STRIKE_HRZ, STRIKE_JAB, PAUSE} STATE;
}
#endif

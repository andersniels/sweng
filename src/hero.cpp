/*
 * hero.cpp
 * Implements the methods of the hero class. The hero class is used for
 * controllable entities
 *
 */

#include "hero.h"
#include "globals.h"
#include <allegro5/allegro_image.h>
#include <iostream>
using namespace std;

#define X_INIT 0
#define Y_INIT 1


Hero::Hero() {
	BITMAP SPRITE = al_load_bitmap(PLAYER_SPRITE_PATH);

	uint8_t iter = 0;
	
	IDLE = al_create_sub_bitmap(SPRITE, 0, 0, WIDTH, HEIGHT);
	STAND = al_create_sub_bitmap(SPRITE, 0, HEIGHT, WIDTH, HEIGHT);
	
	for (iter=0; iter<6; iter++) {
		WALK[iter] = al_create_sub_bitmap(SPRITE, (iter+1)*WIDTH, 0, WIDTH, HEIGHT);
	}
	
	for (iter=0; iter<3; iter++) {
		JUMP[iter] = al_create_sub_bitmap(SPRITE, (iter+7)*WIDTH, HEIGHT, WIDTH, HEIGHT);
		CROUCH[iter] = al_create_sub_bitmap(SPRITE, (iter+7)*WIDTH, 0, WIDTH, HEIGHT);
	}

	/* Req 3.1-1B */
	statusTimer = al_create_timer(0.001);
	al_start_timer(statusTimer);
}

void Hero::updateImage(uint8_t thread) {
	uint64_t statusPersist = al_get_timer_count(statusTimer);
	
	if (moveType == STATUS::STANDING) {
		if (keyState[KEY::LEFT] || keyState[KEY::RIGHT]) {
			currentImage = WALK[(statusPersist/100)%6];
		}
		else if ( statusPersist <= 3000) {
			currentImage = STAND;
		}	
		else {
			currentImage = IDLE;
		}

	}
	/* Jumping sprites */
	else if (moveType == STATUS::JUMPING) {
		if (statusPersist <= 0.4 * jumpTime){
			currentImage = JUMP[0];
		}
		else if (statusPersist <= 0.6 * jumpTime) {
			currentImage = JUMP[1];
		}
		else {
			currentImage = JUMP[2];
		}
	}
	
	else if (moveType == STATUS::CROUCHING) {
		if (statusPersist <= 50) {
			currentImage = CROUCH[0];
		}
		else if (statusPersist <= 75) {
			currentImage = CROUCH[1];
		}	
		else {
			currentImage = CROUCH[2];
		}
	}
}

void Hero::updateStatus(uint8_t thread) {
	
	/* Req 3.1-3B */
	uint64_t statusPersist = al_get_timer_count(statusTimer);
	
	/* Req 3.1-3E */
	if (keyState[KEY::LEFT]) {
		faceDir = DIR::LEFT;
	}
	if (keyState[KEY::RIGHT]) {
		faceDir = DIR::RIGHT;
	}
	
	/* Req 3.1-3A */
	STATUS::STATE moveTypePV = moveType;
	
	/* Req 3.1-3C */
	if (moveType == STATUS::JUMPING) {
		if ( yPos >= statusData[Y_INIT] && statusPersist >= 100) {
			moveType = STATUS::STANDING;
		}
	}
	else if (moveType == STATUS::STANDING) {
		if (keyState[KEY::UP]) {
			moveType = STATUS::JUMPING;

		}
		if (keyState[KEY::DOWN]) {
			moveType = STATUS::CROUCHING;
		}	
	}
	else if (moveType == STATUS::CROUCHING) {
		if (keyState[KEY::UP] || !keyState[KEY::DOWN]) {
			moveType = STATUS::STANDING;
		}
	}
	
	/* Req 3.1-3D */
	if (moveType != moveTypePV) {
		statusData[X_INIT] = xPos;
		statusData[Y_INIT] = yPos;
		al_set_timer_count(statusTimer, 0);
	}
}

void Hero::updatePosition(uint8_t thread) {
	
	/* Req 3.1-2A */
	uint64_t statusPersist = al_get_timer_count(statusTimer);

	/* Req 3.1-2B */
	if (keyState[KEY::LEFT]) {
		xPos -= moveSpeed;
	}
	if (keyState[KEY::RIGHT]) {
		xPos += moveSpeed;
	}
	
	/* Req 3.1-2C */
	if (moveType == STATUS::JUMPING) {
		double gravity = (float)statusPersist / jumpTime;
		yPos = (jumpHeight * 4) * (gravity - 1) * gravity + statusData[Y_INIT];
	}
}

/* Req 2.5-2A, 2.5-2B */
void Hero::setKey(int keycode, bool state) {
	if (keycode == keyMap[KEY::UP]) {
		keyState[KEY::UP] = state;
	} else if (keycode == keyMap[KEY::DOWN]) {
		keyState[KEY::DOWN] = state;
	} else if (keycode ==  keyMap[KEY::LEFT]) {
		keyState[KEY::LEFT] = state;
	} else if (keycode == keyMap[KEY::RIGHT]) {
		keyState[KEY::RIGHT] = state;
	} else if (keycode == keyMap[KEY::BLOCK]) {
		keyState[KEY::BLOCK] = state;
	} else if (keycode == keyMap[KEY::STRIKE_VRT]) {
		keyState[KEY::STRIKE_VRT] = state;
	} else if (keycode == keyMap[KEY::STRIKE_HRZ]) {
		keyState[KEY::STRIKE_HRZ] = state;
	} else if (keycode == keyMap[KEY::STRIKE_JAB]) {
		keyState[KEY::STRIKE_JAB] = state;
	} else if (keycode == keyMap[KEY::PAUSE]) {
		keyState[KEY::PAUSE] = state;
	}
}

BITMAP Hero::getImage() {
	return currentImage;
}

int16_t Hero::getX() {
	return xPos;
}

int16_t Hero::getY() {
	return yPos;
}

int16_t Hero::getDir() {
	return faceDir;
}

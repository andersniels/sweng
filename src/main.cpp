#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <hero.h>
#include <globals.h>


const int SCREEN_W = 640, SCREEN_H = 480;

int main() {
	ALLEGRO_DISPLAY *mainDisplay				= NULL;
	
	/* Req 2.1-1 Initialization */
	const uint8_t schedulerTimerFreq 			= 64;
	ALLEGRO_TIMER *schedulerTimer				= NULL;
	ALLEGRO_EVENT_QUEUE *schedulerEventQueue		= NULL;
	ALLEGRO_EVENT schedulerEvent;
	uint8_t schedulerThread 				= 0;
	
	al_init();
	al_install_keyboard();
	al_init_image_addon();
	
	mainDisplay = al_create_display(SCREEN_W, SCREEN_H);
	
	schedulerTimer 		= al_create_timer((1.0 / schedulerTimerFreq));

	/* Req 2.1-2B */
	schedulerEventQueue = al_create_event_queue();
	al_register_event_source(schedulerEventQueue, al_get_display_event_source(mainDisplay));
	al_register_event_source(schedulerEventQueue, al_get_timer_event_source(schedulerTimer));
	al_register_event_source(schedulerEventQueue, al_get_keyboard_event_source());
	
	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_flip_display();
	
	Hero Player;
	
	/* Req 2.1-2A */
	al_start_timer(schedulerTimer);
	
	
	while(1) {
		/* Req 2.1-3A */
		al_wait_for_event(schedulerEventQueue, &schedulerEvent);
		
		/* Req 2.1-3B */
		if(schedulerEvent.type == ALLEGRO_EVENT_TIMER ) {
			al_clear_to_color(al_map_rgb(0, 0, 0));
			schedulerThread++;
			/* 64 Hz section */
			Player.updateStatus(schedulerThread);
			Player.updateImage(schedulerThread);
			Player.updatePosition(schedulerThread);
			al_draw_bitmap(Player.getImage(), Player.getX(), Player.getY(), Player.getDir());
			/* 32 Hz section */
			if (schedulerThread % 2 == 0) {
				
			}
			/* 16 Hz section */
			if (schedulerThread % 4 == 0) {
				
			}
			al_flip_display();
		}
		/* Req 2.1-3C */
		else if (schedulerEvent.type == ALLEGRO_EVENT_KEY_DOWN) {
			Player.setKey(schedulerEvent.keyboard.keycode, true);
		}
		else if (schedulerEvent.type == ALLEGRO_EVENT_KEY_UP) {
			Player.setKey(schedulerEvent.keyboard.keycode, false);
		}
		
	}
	return 0;
}

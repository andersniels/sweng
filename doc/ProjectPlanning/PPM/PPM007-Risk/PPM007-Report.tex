
\documentclass[openany]{book}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[paperheight=11in,paperwidth=8.5in,margin=1in,heightrounded]{geometry}
\usepackage{array}
\usepackage{lastpage}
\usepackage{outlines}
\usepackage{fancyhdr}
\usepackage{ragged2e}
\usepackage{array}
\usepackage{indentfirst}
\usepackage{float}
\pagestyle{fancy}

\newcolumntype{P}[1]{>{\centering\arraybackslash}m{#1}}
\newcolumntype{l}[1]{>{\raggedright\arraybackslash}m{#1}}

\newcommand{\theRev}{-}
\newcommand{\theDoc}{Risk Management}
\newcommand{\theID}{PPD007}
\begin{document}

\begin{titlepage}

    \begin{center}\vspace*{6cm}
     \textsc{ \Huge
      CPE-425/525 \\
      Risk Management Plan\\
      }
      \large \vspace{1cm}
    \end{center}
    
    \vspace{8cm}
    \begin{flushright}
        Team DAMP\\
        Rev. \theRev
    \end{flushright}
  
\end{titlepage}
  \newpage
  
  \fancyhf{}
  \fancyhead[R]{\theID}
  \fancyfoot[L]{Rev \theRev}
  \fancyfoot[R]{\thepage\ of \pageref{LastPage}}
  \fancyfoot[C]{CPE 425/525 \theDoc}
  \renewcommand{\footrulewidth}{0.2pt}
  \renewcommand{\headrulewidth}{0.2pt}
  \centerline{\Large \textbf{Document Information}}
  
  \vspace{2mm}
  {\center\rule{\linewidth}{0.4mm}}
  \vspace{2mm}
  
  \textbf{Revision History:}
  \begin{center}
      \begin{tabular}{ | P{0.5in} | P{1in} | p{4.47in} | } 
           \hline
           \textit{Rev} & \textit{Date} & \textit{Comment} \\\hline
           Rev \theRev & Oct. 7, 2019 & Initial submission\\\hline
      \end{tabular}
  \end{center}

  \vfill
  \textbf{Proposed by:}
  \begin{center}
      \begin{tabular}{ | m{1.45in} | m{1.45in} | m{1.45in} | m{1.45in} |} 
           \hline
           \textit{Name} & \textit{Title/Role} & \textit{Date} & \textit{Signature} \\\hline
           Anders Nielsen & Technical Lead & & \\[5pt]\hline
           Phillip Raspe & Team Lead & & \\[5pt]\hline
           Matthew Vacca & Developer & & \\[5pt] \hline
           David Labrie & Designer & & \\[5pt]\hline
      \end{tabular}
  \end{center}
  
  \vspace{0.5in}
  \textbf{Approved by:}
  \begin{center}
      \begin{tabular}{ | m{1.99in} | m{1.99in}| m{1.99in} | } 
           \hline
           \textit{Name} & \textit{Date} & \textit{Signature} \\\hline
           Dr. Robert Despang & & \\ [5pt]\hline
      \end{tabular}
  \end{center}
  \vspace{2in}
  
  \newpage
  \section*{Scope}
  This document provides methods for managing risks associated with the project. It outlines the processes for identifying and assessing risks, the pre-event mitigation and post-event response processes, and potential risks of the project.

  \section*{Responsibilities}
  All team members are responsible for identifying and managing risks. All risks must be communicated to the Team Lead, who is responsible for decision making on whether to proceed with mitigation or contingency plans as well as the documentation and tracking of risks.
  
  \section*{Identification}
  When a risk factor or event is discovered, the findings shall be brought to the Team Lead. The Team Lead will then document the risks based upon the following information:
  \begin{outline}
  \1 Description of risk
  \1 Schedule Impact
  \1 Scope Impact
  \1 Quality Impact
  \1 Cost Impact
  \end{outline}

  \section*{Assessment}
  After a risk is identified, it will be assessed and categorized based on the probability of the risk occurring and the severity that the risk will have on the project should it occur as defined below.
  \begin{outline}
  \1 Likeliness of Occurrence:
    \2 (5) Frequent - 100-90\% chance of occurring per week
    \2 (4) Likely - 89-60\% chance of occurring per week
    \2 (3) Occasional - 59-40\% chance of occurring per week
    \2 (2) Seldom - 39-10\% chance of occurring per week
    \2 (1) Improbable - 9-0\% chance of occurring per week
  \1 Severity of Occurrence:
    \2 (A) Catastrophic - will halt all project progress if it occurs
    \2 (B) Critical - will delay the the long term project plan and delivery date
    \2 (C) Moderate - will cause continual and escalating short-term disruptions if left unattended
    \2 (D) Minor - will not directly affect the project plan, but may affect efficiency of resources
    \2 (E) Negligible - Will not affect the project, team, or resources in a quantifiable manner 
  \end{outline}
  Once the a risk is evaluated, it will be classified into high, medium, and low risk levels. These are based off their cumulative severity and likelihood per the following tables:\\
  \includegraphics[width=\linewidth]{likeliness-severity.PNG}\\\vspace{1cm}\\
  \includegraphics[width=\linewidth]{risk-level.PNG}
  \section*{Response}
  Appropriate responsive measures shall be implemented based on the risk assessment. Responsive measures will be considered in the following order on a case-by-case system:
  \begin{outline}
  \1 Avoidance - Change the project to avoid the risk.
  \1 Mitigation - Take steps to reduce the probability or impact of the risk.
  \1 Acceptance - Accept the risk and the consequences that may come with it
  \1 Deferred - Determine that the risk will be addressed at a later time
  \end{outline}
  The mitigation process will include the identification and adaptation of other activities or processes that affect the probability or severity of the risk. A contingency plan will also be created to handle each risk if they should occur.
  \section*{Risks}
  \subsection*{Technical Risks}
  \begin{outline}
\1 Large project, small team with little experience of entire software engineering process
\2 Schedule risk: underestimates of time to complete tasks
\2 Scope impact: greatly slow down process because of fixing mistakes and
learning on the job
\2 Quality impact: quality of product could diminish due to time constraints and
lack of experience i.e. skipping some quality control to produce the product on
time
\2 Cost: much more money and dev time if product is not delivered on time
\2 Level: 3B
\2 Response : Deferred
\1 Some members of the team have little experience with OS and lack of and IDE
\2 Schedule: training could back up development time
\2 Scope: could impact complexity and capabilities of project
\2 Quality: could fail to meet customer requirements due to lack of experience
\2 Cost: more time spent on training and learning than planned for
\2 Level: 4B
\2 Response: Mitigation
\3 Team members who have experience will guide inexperienced
members
\3 Contingency Plan: Have inexperienced members shadow experienced
members so they can learn. This will take them off productive aspects
of the project but there will be a better understanding of what’s going
on and fewer errors in the future.
\end{outline}
\subsection*{Management Risks}
\begin{outline}
\1 Insufficient scheduling due to lack of experience
\2 Schedule: Could incorrectly allot time for each step of process
\2 Scope: Limit complexity of project due to mismanaged time
\2 Quality: not enough time alloted for quality control
\2 Cost: cost time in every step of process
\2 Level: 4D
\2 Response: Acceptance
\1 Insufficient testing requirements
\2 Schedule: spend more time on testing and quality than originally planned
\2 Scope: Limit capabilities of game due to more defects
\2 Quality: More defects will be produced
\2 Cost: More time and resources needed to fix defects
\2 Level: 2B
\2 Response: Acceptance
\1 Insufficient quality assurance
\2 Schedule: product functions incorrectly and majority of process will be redone
\2 Scope: More possible features, much more possible defects
\2 Quality: will be bad
\2 Cost: money and time to redo process
\2 Level: 2E
\2 Response: Acceptance
\1 Lack of role management
\2 Schedule: Role specific tasks may get delayed or done twice
\2 Scope: Limit complexity of game
\2 Quality: Lack of following quality procedures
\2 Cost: Time
\2 Level: 4D
\2 Response: Acceptance
\1 Miscommunication
\2 Schedule: Unsure what tasks are done or in progress
\2 Scope: Likely to reduce capabilities of game
\2 Quality: Quality checks that need to made may be misinterpreted by others
\2 Cost: development time
\2 Level: 3A
\2 Response: Mitigation
\3 Keep open communication on what everyone is doing
\3 Have a group chat where all team members can stay in contact and
voice concerns
\3 Contingency Plan: Have in person meeting where we address any
problems on miscommunication between group members. Possibly
include upper management (Dr. Despang) if the miscommunication is
bad enough.
\end{outline}
\subsection*{Personnel Risks}
\begin{outline}

\1 Team member injury
\2 Schedule: Tasks take longer with less members
\2 Scope: Less features due to member constraints
\2 Quality: Less man hours for quality assurance
\2 Cost: More time for other team members
\2 Level: 1A
\2 Response: Mitigation
\3 Have members be constantly working together so if one member
cannot continue with the project, others can step in and pick up where
they left off.
\3 Have members keep logs of what they are doing.
\3 Contingency Plan: Halt project and regroup.
\1 Bad relations between team members
\2 Schedule: Conflicting views on time alloted for tasks
\2 Scope: Lack of teamwork could affect complexity of game
\2 Quality: could cause less concern about the quality of the game in a bad
environment
\2 Cost: Longer to coordinate meeting and task deliberation
\2 Level: 1B
\2 Response: Mitigation
\3 Have open communication and address any bad relations
\3 Try to mediate any bad relations and keep people with problems
separate as much as possible
\3 Contingency Plan: Separate members with bad relations completely
\end{outline}
\end{document}

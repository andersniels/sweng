Installation  
============  
1. Windows  
	1. Choose download link for your system  
	2. Run application, choose defaults for everything  
2. Linux  
	1. Open terminal  
	2. Run 'sudo apt-get install git'  
	3. Type 'Y' and <ENTER> when prompted  

Getting Started  
===============  
1. Open Linux Terminal or git bash (Windows)  
2. Navigate to where you want to put the repository  
3. Type 'git clone https://andersniels@bitbucket.org/andersniels/sweng.git'  
4. Type 'cd sweng'  

General Use  
===========  

There are two branches on the remote repository, the master and dev. Development will be done on dev. Perform these steps when you are ready to push to the development branch. 

1. run 'git checkout dev'  
2. run 'git fetch'  
3. run 'git merge origin/dev'  
4. make edits  
5. run 'git add <changed files>'  
6. run 'git commit -m "<your commit message>"'  
7. run 'git push -u origin dev'  


Cheat Sheet  
===========  
https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet  

- git branch  
	- -a  = show all branches  
	- -r  = show remote branches  
	- -l  = show local branches  
	- <branch name> = create local branch <branch name> 
		- git checkout <branch name> = to move to that branch
		- git checkout -b <branch name> = git branch and git checkout

- git status  
	- show list of changes  

- git add 
	- <file name> = add changes to <file name>  
	- . = add all changes  
